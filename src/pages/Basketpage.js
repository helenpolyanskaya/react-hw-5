import React from 'react';
import Cards from "../components/Cards";
import { useSelector } from "react-redux";
import Form from '../components/Form';


export default function Basketpage () {
    
    const cards = useSelector((state) => state.basketReducer.cards);

    return (
        cards.length>0 ?  <><Form/><Cards cards = {cards} 
        context= {'Remove from basket'}
        /> </>:  <h1> "В корзині товарів немає!"</h1>
           
    ) 
       
    
}
