import { basketTypes } from "../types";

export function addToBasket (card) {
     return async function (dispatch) {
          dispatch({
               type: basketTypes.BASKET_ADD_CARD,
               payload: card
          })
     }
}

export function loadBasket () {
     return async function(dispatch){
          const basketArrayRaw = localStorage.getItem (['Toys basket']) ;
          const basketArray  = basketArrayRaw ? JSON.parse(basketArrayRaw) : [];
          dispatch({
               type: basketTypes.LOAD_BASKET,
               payload: basketArray,
          })
     }
}

export function deleteToBasket (card) {
     return {
          type: basketTypes.BASKET_DELETE_CARD,
          payload: [card]
     }
}

export function unloadBasket (cards) {
     return {
     
               type: basketTypes.UNLOAD_BASKET
     }
}

