import React from 'react';
import { useFormik} from "formik";
import './form.css';
import * as Yup from 'yup';
import { useDispatch, useSelector } from "react-redux";
import { unloadBasket } from '../../redux/actions/basket';
import { PatternFormat } from 'react-number-format';

export default function Form () {

    const basketCards = useSelector((state) => state.basketReducer.cards);
    const dispatch = useDispatch();
    //const valuesCards = [];

    const validate = values => {
        const errors = {};
        if (!values.name) {
            errors.name = 'Please, fill in the field!'}
        else if (!/^[a-zA-Z]+$/i.test(values.name)) {
            errors.name =  'Only latin letters!'
        }  
                    
        if (!values.lastname) {
            errors.lastname =  'Please, fill in the field!'}
        else if (!/^[a-zA-Z]+$/i.test(values.lastname)) {
            errors.lastname =  'Only latin letters!'}           


        if (!values.age) {
            errors.age =  'Please, fill in the field!'}
        else if (/^[a-zA-Z]+$/i.test(values.age)) {
            errors.age =  'Only numbers!'}           
    
        if (!values.address) {
            errors.address =  'Please, fill in the field!'}   

        if (!values.phone) {
            errors.phone =  'Please, fill in the field!'}
        else if (/^[a-zA-Z]+$/i.test(values.phone)) {
            errors.phone =  'Only numbers!'}      
        
        return errors;    
    };

   


    const formik = useFormik({
        initialValues:{
            name: '',
            lastname: '',
            age:'',
            address:'',
            phone:'',
        },
        validate,
        validationSchema: Yup.object({
            name: Yup.string()
              .max(15, 'Must be 15 characters or less')
              .required('Required'),
            lastname: Yup.string()
              .max(20, 'Must be 20 characters or less')
              .required('Required'),
            age: Yup.number()
                .min(18, 'Only for adults!')
                .max(99, 'Must be 2 characters or less')
                .required('Required'),
            address: Yup.string()
                .max(50, 'Must be 50 characters or less')
                .required('Required'),   
            phone: Yup.string()
                .matches(/^(\+\(380\))([0-9]{3}\-[0-9]{3}\-[0-9]{3})$/gm,"Incorect phone")
                .required('Required'),     
          }),
        
        onSubmit: values => {
            console.log('Client: ', values);
            console.log('Order:', basketCards);
            dispatch(unloadBasket(basketCards))
          }
        
    });

    return(

        <form onSubmit={formik.handleSubmit}>
            <h1>To place an order fill in the fields</h1>
            <div className='form_wrapper'>
                <div className='form'>
                    <label htmlFor='name'>Add your name: </label>
                    <input id='name'
                     name='name' 
                     type='text'
                     onChange={formik.handleChange}
                     onBlur={formik.handleBlur}
                     value={formik.values.name}/>
                     {formik.touched.name && formik.errors.name ? (<div className='errors'>{formik.errors.name}</div>) : null}
                     <br></br>
                     <label htmlFor='lastname'>Add your lastname: </label>
                     <input id='lastname'
                     name='lastname' 
                     type='text'
                     onChange={formik.handleChange}
                     onBlur={formik.handleBlur}
                     value={formik.values.lastname}/>
                     {formik.touched.lastname && formik.errors.lastname ? (<div className='errors'>{formik.errors.lastname}</div>) : null}
                     <br></br>
                     <label htmlFor='=age'>Add your age: </label>
                     <input id='age'
                     name='age' 
                     type='text'
                     onChange={formik.handleChange}
                     onBlur={formik.handleBlur}
                     value={formik.values.age}/>
                     {formik.touched.age && formik.errors.age ? (<div className='errors'>{formik.errors.age}</div>) : null}
                     <br></br>
                     <label htmlFor='adress'>Add delivery address: </label>
                     <input id='adress'
                     name='address' 
                     type='text'
                     onChange={formik.handleChange}
                     onBlur={formik.handleBlur}
                     value={formik.values.address}/>
                     {formik.touched.address && formik.errors.address ? (<div className='errors'>{formik.errors.address}</div>) : null}
                     <br></br>
                     <label htmlFor='phone'
                     >Add your phone number:  
                     </label>
                     <PatternFormat format="+(380)###-###-###" allowEmptyFormatting mask="_"
                     id='phone'
                     name='phone' 
                     type='text'
                     onChange={formik.handleChange}
                     onBlur={formik.handleBlur}
                     value={formik.values.phone}/>
                    {formik.touched.phone && formik.errors.phone ? (<div className='errors'>{formik.errors.phone}</div>) : null}
                    <div className='buttonForm'>
                        <button type='submit' 
                        disabled={!formik.isValid}
                        style={{
                            color: !formik.isValid ? 'rgb(81, 79, 79)' : 'white',
                            backgroundColor: !formik.isValid ? 'rgb(146, 143, 143)' : 'rgb(189, 189, 189)', 
                            padding: '10px 15px',
                        }}
                        >Checkout</button>
                    </div>
                 </div>
             </div>
                           
        </form>
    )

}