import React from 'react';
import basketIcon from '../../image/basketIcon.png';
import './basket.css';
import PropTypes from 'prop-types';
import { useSelector } from "react-redux";

export default function Basket() {

    const cards = useSelector((state) => state.basketReducer.cards);
    

    return(
        <div className='basket_wrapper'>
            <a className='basket-icon'>
                <img src={basketIcon}></img>
            </a>
            <div className='basket-count'>
                {cards.length}
            </div>
        </div>
    )
}


Basket.propTypes = {
    basket: PropTypes.array,
};


