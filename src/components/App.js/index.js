import React, { useState, useEffect} from "react";
import Basketpage from "../../pages/Basketpage.js";
import Favoritepage from "../../pages/Favoritepage.js";
import { NavLink, Routes, Route } from "react-router-dom";
import Modal from '../Modal';
import Cards from '../Cards';
import Basket from '../Basket';
import iconstore from '../../image/iconstore80.png';
import Favorite from '../Favorite';
import {deleteToBasket} from '../../redux/actions/basket';
import { loadFavorites } from '../../redux/actions/favorite';
import { loadBasket } from '../../redux/actions/basket';
import { useDispatch, useSelector } from "react-redux";
import { fetchCardsAsync } from "../../redux/actions/cards.js";

import './app.css';

export default function App() {
    const dispatch = useDispatch();

    const [openModal, setOpenModal] = useState (false);
    const [modal, setModal] = useState ({});

    const closeModal = () => {
        setOpenModal(false)
    }

    const deleteBasket = (card) => {
        dispatch(deleteToBasket(card))

    }  

    const cards = useSelector((state) => state.cardsReducer.cards);

    useEffect (() => {       
        dispatch(fetchCardsAsync());

        dispatch(loadBasket());

        dispatch(loadFavorites());
    }, [])


    return (
        <>  
            <div className='nav_wrapper'>
                <NavLink to="/">
                    <img src={iconstore} alt="Toys"></img>
                </NavLink>    
                <div className='counts_wrapper'>
                    <NavLink to="/favorite">
                        <Favorite/>
                    </NavLink>
                    <NavLink to="/basket">
                        <Basket/>
                    </NavLink>    
                </div>  
            </div>      
            <Routes>
                <Route path="/" 
                    element={
                        <Cards
                        cards = {cards}
                        context = {'Add to basket'}
                        modalId = 'addCard'
                        />     
                }/>
                <Route path="/basket" 
                    element={<Basketpage
                        modalId = 'editBasket'
                        page="basket"
                    />
                }/>
                <Route path="/favorite" 
                    element={<Favoritepage
                        context = {'Add to basket'}
                        modalId = 'addCard'
                        page="basket"
                    />
                }/>
            </Routes>
            <Modal/>
        </>
    )
}
