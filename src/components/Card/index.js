import './card.css';
import Button from '../Button';
import starunfilled from '../../image/starunfilled.png';
import starfilled from '../../image/starfilled.png';
import PropTypes from 'prop-types';
import { useDispatch, useSelector} from "react-redux";
import { addToFavorite } from '../../redux/actions/favorite'
import { openAddToBasketModal, openDeleteBasketModal } from '../../redux/actions/modal';

export default function Card ({card, context}) {
    const dispatch = useDispatch();
    const favoriteCards = useSelector((state) => state.favoriteReducer.cards)
    const favorite = favoriteCards.map(c => c.article).includes(card.article)
    const basketCards = useSelector((state) => state.basketReducer.cards)
    const basketMode = context == 'Remove from basket';
    const inBasket = !basketMode && basketCards.map(c => c.article).includes(card.article)

       const addToFavoritesClick  = () => {
         dispatch(addToFavorite(card))
       }

    const openModal = () => {
        dispatch(basketMode ?  openDeleteBasketModal(card) : openAddToBasketModal(card))
    }

        return(
            <div className='card'>
                <div style={{textAlign: 'center', marginTop: '15px'}}>
                    <img src={card.url} className='imgCard'></img>
                </div>
                <h1>{card.name}</h1>
                <h3>Price: {card.price}.00 euro</h3>
                <hr/>
                <p>Article: {card.article}</p>
                <p>Color: {card.color}</p>
                <div className='add_wrapper'>
                    <div className='button_wrapper'>
                        <Button disabled={inBasket} text={context} onClick={openModal}  card={card} backgroundColor='#bdbdbd'/>
                    </div>
                    <div>
                        <a onClick={addToFavoritesClick} >
                            <img src=  {!favorite ? starunfilled : starfilled } > 
                            </img> 
                        </a>
                    </div>
                </div>

            </div>
        )
    
}


Card.propTypes = {
    name: PropTypes.string,
    article: PropTypes.number,
    price: PropTypes.number,
    url: PropTypes.string,
    color: PropTypes.string
};

Card.defaultProps = {
    article: 0,
};



