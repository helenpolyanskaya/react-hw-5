import React from 'react';
import Card from '../Card';
import './cards.css';
import PropTypes from 'prop-types';


export default function Cards({cards, context}) {

 
    return(
        <div className='cards_wrapper'>{cards.map((elem) => {
            return(
                <Card 
                    key = {elem.article}
                    card={elem} 
                    context= {context}
                />
            )})} 
        </div>
    )
}

Cards.propTypes = {
    favoriteCards: PropTypes.array,
    clickOpenModal: PropTypes.func,
}
