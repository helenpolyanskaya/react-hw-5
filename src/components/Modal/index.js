import React from 'react';
import './modal.css';
import PropTypes from 'prop-types';
// import { useEffect, useState } from 'react';
import { useSelector } from "react-redux";
import { closeModalAction } from '../../redux/actions/modal';
import { addToBasket , deleteToBasket } from '../../redux/actions/basket';
import { useDispatch } from 'react-redux';

export default function Modal() {
    const open = useSelector((state) => state.modalReducer.open);
    const card = useSelector((state) => state.modalReducer.card);
    const dispatch = useDispatch();

    const modalId = useSelector((state) => state.modalReducer.modalId);

    console.log(modalId);
   const deleteToBasketClick = function() {
    dispatch(closeModalAction());
    dispatch(deleteToBasket(card)) 
   }

    const addToBasketClick = function() {
        dispatch(closeModalAction());
        dispatch(addToBasket(card));
    }
    const closeModal = () => {
        dispatch(closeModalAction())
    }
    const cardName = card ? card.name : "";

    const modalsTypes = {
        editBasket: {
            header: "Do you want to delete this file?",
            text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want delete it?",
            actions1: "OK",
            actions2: "Cancel",
            onAction1: deleteToBasketClick,
            onAction2: closeModal,
        }, 

        addCard: {
            header: "In the basket: " + cardName,
            context: "Add to basket",
            text: "Are you sure you want add it?",
            actions1: "Confirm",
            actions2: "Cancel",
            onAction1: addToBasketClick,
            onAction2: closeModal,
        },
    }

    const showClass = () => {
        if(open === false){
            return 'off';
        }   
        return "";   
    }


        if( open === false){
            return null;
        }

        return (
         <div  id='modal_wrapper' onClick={(e) => e.target === e.currentTarget && closeModal()}>
            <div className={`modal ${showClass()}`}>
                <div className='header_wrapper'>
                    <h2>{modalsTypes[modalId].header}</h2>
                    <button className='toggle-button' onClick={closeModal}>
                        X
                    </button>
                </div>
                <div className='content'>
                    <h3>{modalsTypes[modalId].text}</h3>
                    <button onClick = {modalsTypes[modalId].onAction1} style={{border: '1px solid rgb(81, 79, 79)'}}>
                        {modalsTypes[modalId].actions1}
                    </button>
                    <button onClick = {modalsTypes[modalId].onAction2} style={{border: '1px solid rgb(81, 79, 79)'}}>
                        {modalsTypes[modalId].actions2}
                    </button>
                </div>
            </div>   
         </div>
            
        )
    }

Modal.propTypes = {
    actions1: PropTypes.string,
    actions2: PropTypes.string,
  };



 
