import React from 'react';
import starunfilled from '../../image/starunfilled.png';
import './favorite.css';
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";

export default function Favorite() {

    const favoriteCards = useSelector((state) => state.favoriteReducer.cards)

   return(
    <>
    <div className='favorite_wrapper'>
            <NavLink>
                <img src={starunfilled}></img>
            </NavLink>
            <div className='favorite-count'>
                {favoriteCards.length}
            </div>
        </div>
        </>
        
    )
}

