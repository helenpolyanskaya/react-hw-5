//import { render } from "@testing-library/react";
import React from 'react';
import './button.css';
import PropTypes from 'prop-types';


export default function Button({modalId, disabled, card, onClick, text, backgroundColor}) {

    const openModal = () => {
        onClick(modalId, card);
    }

    return(
        <button disabled={disabled} onClick={openModal} 
        style={{
                color: disabled ? 'rgb(81, 79, 79)' : 'white',
                backgroundColor: disabled? 'rgb(146, 143, 143)' : backgroundColor, 
                padding: '10px 15px',
            }} >
            {text}
        </button>
    )
}


Button.propTypes = {
    text: PropTypes.string,
    card: PropTypes.object,
  };

Button.defaultProps = {
    backgroundColor: 'rgb(238, 238, 238)',
};